# Arthur David Müzel Calciolari

## About me
Hello there! my name is Arthur calciolari. I'm interested in finance, black holes, the bleeding edge of tecnology and HEAVY-METAL 🤘



### Here's what i've been working on the past few months (mainly university stuff):
- https://gitlab.com/arthurcalciolari/uni-jalauniversity

## Feel free to contact me:
- E-mail: arthur_calciolari@outlook.com
- LinkedIn: https://www.linkedin.com/in/arthur-calciolari-078138224/
- GitLab: https://gitlab.com/arthurcalciolari

## Languages
- Java
- Python
- PostgresSQL
- MySQL
- Git (sort of)

## GitLab stats
<div align="center">
  <img height="150em" src="https://github-readme-stats.vercel.app/api/top-langs/?username=artcalciolari&layout=compact&langs_count=7&theme=bright"/>
</div>

